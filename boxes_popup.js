(function($) {

Drupal.boxes_popup_form = Drupal.boxes_popup_form || {};
Drupal.boxes_popup_form.submit = Drupal.boxes_popup_form.submit || {};

Drupal.behaviors.boxes_popup = {
  attach : function(context) {
    var $context = $(context);
  
    for (var delta in Drupal.settings.boxes_popup) {
  
      var actions = Drupal.settings.boxes_popup[delta]['actions'];
      var popup = (actions.indexOf('hide', 0) !== -1);
      var hide = (actions.indexOf('hide', 0) !== -1);
      var fallback = (actions.indexOf('fallback', 0) !== -1);
      var show_cross = Drupal.settings.boxes_popup[delta]['show_cross'];
  
      $context.find("[id*='"+delta+"']").each(function() {
        $this = $(this);
        if ($this.hasClass('block') || $this.hasClass('boxes') || $this.hasClass('boxes-box')) {
          if (hide) {
            $this.css('display', 'none');
          }
          else {
            $this.show();
          }
          if (popup) {
            Drupal.CTools.Modal.show();
            $('#modal-title').html('Disclaimer');
            $('#modal-content').html(Drupal.settings.boxes_popup[delta]['content']);
            $('#modalContent').addClass('boxes-popup');
            if (show_cross == 0) {
              $('#modalContent .modal-header a.close').hide();
            }
  
            // Resize based on the dimensions of our content
            // This doesn't seem to work unless we wait a while
            // @TODO: Work out why
            $('.ctools-modal-content').css('display', 'none');
            setTimeout(function() {$('.ctools-modal-content').show(); $(window).resize();}, 250);
  
            // For boxes_popup_forms
            // Fire a hook onSubmit
            $('#modal-content form').each(function() {
              $form = $(this);
              $form.find("[type=submit],[type=image]").bind("click", function(e){
                $(this).attr("was_clicked_to_submit","YES");
              });
              $form.submit(function() {
                // To get called when a form in the popup is submitted
                // implement the Drupal.boxes_popup_form.submit.<hook> ($form)
                // - return false to indicate that the form should not be
                // submitted but instead that the popup should just be closed
                var return_false = false;
                jQuery.each(Drupal.boxes_popup_form.submit, function() {
                  if (this($form) === false) {
                    Drupal.CTools.Modal.dismiss();
                    return_false = true;
                  }
                });
                if (return_false) {
                  Drupal.CTools.Modal.dismiss();
                  return false;
                }
                return true;
              });
            });
          }
  
        }
      });
    }
  }
}

})(jQuery);
