<?php

$box = boxes_factory('popup');
$box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
$box->api_version = 1;
$box->delta = 'example_popup';
$box->plugin_key = 'popup';
$box->title = 'My Popup Box';
$box->description = 'My Popup Box';
$box->options = array(
  'body' => '<p>Lorem ipsum..</p>',
  'format' => '1',
);