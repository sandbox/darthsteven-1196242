<?php

/**
 * Simple custom text box that can be displayed as a popup or not
 */
class boxes_popup extends boxes_simple {
  /**
   * Implementation of boxes_content::options_defaults().
   */
  public function options_defaults() {
    $options_defaults = parent::options_defaults();
    $options_defaults['popup_show_cross'] = '1';
    $options_defaults['popup_display'] = 'replace-with-fallback';
    return $options_defaults;
  }

  /**
   * Implementation of boxes_content::options_form().
   */
  public function options_form() {

    $form = parent::options_form();

    $form['popup_display'] = array(
      '#type' => 'select',
      '#title' => t('Display popup'),
      '#default_value' => (!empty($this->options['popup_display']) ? $this->options['popup_display'] : 'popup hide fallback'),
      '#options' => array (
        'popup hide fallback' => 'Instead of the normal box (with fallback)',
        'popup hide' => 'Instead of the normal box (no fallback)',
        'popup' => 'As well as the normal box',
      ),
      '#description' => t("The popup be displayed instead of the box (which will be hidden), or it can be displayed while keeping the box in it's normal place. If 'with fallback' is selected, the box will be displayed in it's normal location only if the user does not have javascript"),
    );

    $form['popup_show_cross'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show close button'),
      '#default_value' => (!empty($this->options['popup_show_cross']) ? $this->options['popup_show_cross'] : 1),
      '#description' => t("NOTE: If this is disabled, there will be no way for the user to close the popup - you must provide the alternative - some sort of link inside the block."),
    );

    return $form;
  }

  /**
   * Implementation of boxes_content::render().
   */
  public function render() {

    $block = parent::render();

    $popup_display = (!empty($this->options['popup_display']) ? $this->options['popup_display'] : 'popup hide fallback');

    // Merge in our classes with whatever our parent may have added
    $classes = array('boxes-popup-' . $popup_display);
    $block['classes'] = array_merge($classes, (isset($block['classes']) ? $block['classes'] : array()));

    // Add the js/css we need
    $this->add_js_css($block['subject'], $this->render_self($block));

    // If there is no fallback, we need to return nothing for the content of the block
    if ((strpos($popup_display, 'fallback') === FALSE)) {
      $block['content'] = $block['subject'] = '';
    }

    return $block;
  }

  /**
   * Includes all the js & css we need to work
   * correctly. The final themed output of the block
   * is needed so we can send it to the page, even when
   * a fallback has been declined (and so the box is not
   * present on the page any other way)
   */
  public function add_js_css($block_title, $themed_content) {
    ctools_include('modal');
    ctools_modal_add_js();

    $path = drupal_get_path('module', 'boxes_popup');

    drupal_add_js($path . '/boxes_popup.js');
    drupal_add_css($path . '/boxes_popup.css');

    $setting['boxes_popup'][$this->delta]['actions'] = (!empty($this->options['popup_display']) ? $this->options['popup_display'] : 'popup hide fallback');
    $setting['boxes_popup'][$this->delta]['show_cross'] = (!empty($this->options['popup_show_cross']) ? $this->options['popup_show_cross'] : 0);
    $setting['boxes_popup'][$this->delta]['title'] = $block_title;
    $setting['boxes_popup'][$this->delta]['content'] = $themed_content;
    drupal_add_js($setting, 'setting');
  }

  /**
   * We sneakily copy some code from boxes module so we
   * can produce the final html version of our content
   */
  protected function render_self($block) {
    // The nice ajaxy inline editing won't work in the popup,
    // so we exclude it here
    //if (boxes_access_admin()) {
    //  $block['controls'] = theme('links', array(
    //    'edit' => array(
    //      'title' => t('Edit'),
    //      'href' => $_GET['q'],
    //      'query' => 'plugin_key=&boxes_delta=' . $block['delta'],
    //      'attributes' => array('class' => 'ctools-use-ajax'),
    //    ),
    //    'cancel' => array('title' => t('Cancel'), 'href' => $_GET['q']),
    //  ));
    //}
    $block['content'] = theme('boxes_box', array('block' => $block));
    return $block['content'];
  }
}
