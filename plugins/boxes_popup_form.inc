<?php

/**
 * Extends popups so forms can be embedded within
 */
class boxes_popup_form extends boxes_popup {
  /**
   * Implementation of boxes_content::options_defaults().
   */
  public function options_defaults() {
    $options_defaults = parent::options_defaults();
    
    $options_defaults['form'] = '';
    
    return $options_defaults;
  }

  /**
   * Implementation of boxes_content::options_form().
   */
  public function options_form() {
    
    $form = parent::options_form();
    
    $form['form'] = array (
      '#type' => 'textfield',
      '#title' => t('Form'),
      '#default_value' => $this->options['form'],
      '#description' => 'Field constructor hook',
    );
    
    $form['body']['#title'] = t('Scope note');
    
    return $form;
  }

  public function render() {
    
    $block = parent::render();
    
    $popup_display = (!empty($this->options['popup_display']) ? $this->options['popup_display'] : 'popup hide fallback');
    if ((strpos($popup_display, 'fallback') !== FALSE)) {
      $block['content'] = $this->get_content($block, FALSE);
    }
    
    
    return $block;
  }
  
  protected function render_self($block) {
    $block['content'] = $this->get_content($block);
    $block['content'] = theme('boxes_box', array('block' => $block));
    return $block['content'];
  }
  
  protected function get_content($block, $ajax = FALSE) {
    $output = '';
    $output .= $block['content'];
    if (!empty($this->options['form'])) {
      $output .= drupal_render(drupal_get_form($this->options['form']));
    }
    return $output;
  }
}
